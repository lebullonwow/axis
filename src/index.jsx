import React from 'react';
import ReactDOM from 'react-dom';
import HelloWorld from './components/HelloWorld';
import AxisWorld from './components/AxisWorld';
import './styles/app.scss';
ReactDOM.render(
   <AxisWorld />,
   document.getElementById('app')
);